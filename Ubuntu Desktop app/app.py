from threading import Thread
from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtCore import QDir, Qt, QUrl, QSize, QObject, pyqtSignal, QThread
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QHBoxLayout, QLabel,
                             QPushButton, QSizePolicy, QSlider, QStyle, QVBoxLayout, QWidget, QStatusBar, QMainWindow,
                             QDesktopWidget, QStackedLayout)
from PyQt5 import QtCore
from PyQt5.uic.properties import QtGui

from duration import durationChek  #This is to measure duration of video
import random
import os
import subprocess
import time

class SecondWindow(QWidget):
    def __init__(self):
        super().__init__()
        layout = QVBoxLayout()

        self.label = QLabel("Another Window % d" % random.randint(0, 100))
        self.setGeometry(0,0,600,400)
        self.setWindowTitle('Second Window')
        self.pushBtn1 = QPushButton("Run Bash File1",self)
        self.pushBtn2 = QPushButton("Run Bash File2", self)
        self.pushBtn1.move(250, 150)
        self.pushBtn2.move(250, 200)
        self.pushBtn1.clicked.connect(self.fun_bash1)
        self.pushBtn2.clicked.connect(self.fun_bash2)

        #staylesheet
        self.setStyleSheet("background-color: black")
        self.pushBtn1.setStyleSheet("border: 2px solid white; color: white;padding: 2px 2px 2px 2px;")
        self.pushBtn2.setStyleSheet("border: 2px solid white;color: white; padding: 2px 2px 2px 2px;")

        layout.addWidget(self.pushBtn1)
        layout.addWidget(self.pushBtn2)



        self.initUI() #to open window in center of the screen

    # to open window in center of the screen
    def initUI(self):
        self.setWindowTitle('Center the app')
        self.center()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def fun_bash1(self):
        subprocess.call(['sh', 'bash1.sh'])

    def fun_bash2(self):
        subprocess.call(['sh', 'bash2.sh'])

class Worker(QObject):
    finished = pyqtSignal()
    progress = pyqtSignal(int)

    def run(self):
        start_time = time.time()
        print("Start time", start_time)
        while True:
            end_time = time.time()
            if end_time - start_time >= 8:
                break
        self.finished.emit()

class video_player(QWidget):
    # Video Player Main widget
    def __init__(self):
        super().__init__()
        self.second_call = SecondWindow()
        self.mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        # btnSize = QSize(16, 16)
        videoWidget = QVideoWidget()
        self.counter = True #flag to check mouseDoubleClick to full screen
        self.Video()
        self.positionSlider = QSlider(Qt.Horizontal)
        self.positionSlider.setRange(0, 0)
        # self.positionSlider.sliderMoved.connect(self.setPosition)

        # controlLayout = QHBoxLayout()
        # controlLayout.setContentsMargins(0, 0, 0, 0)
        # controlLayout.addWidget(self.positionSlider)

        # layout = QVBoxLayout()
        layout = QStackedLayout()
        layout.addWidget(videoWidget)
        # layout.addWidget(controlLayout)
        layout.setCurrentIndex(1)
        self.setLayout(layout)

        self.mediaPlayer.setVideoOutput(videoWidget)
        self.mediaPlayer.positionChanged.connect(self.positionChanged)
        self.mediaPlayer.durationChanged.connect(self.durationChanged)

        self.initUI() #to open window in center of the screen

    # to open window in center of the screen
    def initUI(self):
        self.center()
        # self.show()

    def center(self):
        qr = self.frameGeometry()
        cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())

    def mouseDoubleClickEvent(self, event):
        if  self.counter:
            self.counter = False
            self.showFullScreen()
            # self.positionSlider.hide()
        elif self.counter==False:
            self.showNormal()
            # self.positionSlider.show()
            self.counter = True

    def Video(self):
        # fileName, _ = QFileDialog.getOpenFileName(self, "Selecciona los mediose",
        #         ".", "Video Files (*.mp4 *.flv *.ts *.mts *.avi)")
        fileName = "/home/wot-param/Documents/Param/weboccult.mp4"

        if fileName != '':
            self.mediaPlayer.setMedia(
                QMediaContent(QUrl.fromLocalFile(fileName)))
            self.play()

    def play(self):
        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.mediaPlayer.pause()
        else:
            self.mediaPlayer.play()

    def positionChanged(self, position):
        self.positionSlider.setValue(position)
        if durationChek("/home/wot-param/Documents/Param/weboccult.mp4") <= position:
            self.hide()
            self.second_call.show()
            self.thread = QThread()
            self.worker = Worker()
            self.worker.moveToThread(self.thread)
            self.thread.started.connect(self.worker.run)
            self.worker.finished.connect(self.thread.quit)
            self.thread.start()
            self.thread.finished.connect(self.second_call.close)
            self.thread.finished.connect(self.show)
            self.thread.finished.connect(self.Video)
        else:
            None

    def check_for_second_window(self):
        start_time = time.time()
        print("Start time", start_time)
        while True:
            end_time = time.time()
            if end_time - start_time >= 8:
                self.second_call.close()
                self.show()
                break

    def durationChanged(self, duration):
        self.positionSlider.setRange(0, duration)

    def setPosition(self, position):
        self.mediaPlayer.setPosition(position)

#Media player main Widget
if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    player = video_player()
    player.setWindowTitle("Player")
    player.resize(600, 400)
    player.show()
    sys.exit(app.exec_())
