from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtCore import QDir, Qt, QUrl, QSize
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QHBoxLayout, QLabel,
                             QPushButton, QSizePolicy, QSlider, QStyle, QVBoxLayout, QWidget, QStatusBar, QMainWindow)
from PyQt5 import QtCore
from duration import durationChek  #This is to measure duration of video
import random
import os

class SecondWindow(QWidget):
    def __init__(self):
        super().__init__()
        layout = QVBoxLayout()

        self.label = QLabel("Another Window % d" % random.randint(0, 100))

        self.setWindowTitle('Second Window')
        self.pushBtn1 = QPushButton("Run Bash File1",self)
        # self.pushBtn1.setStyleSheet("border: 1px solid white;")
        self.pushBtn2 = QPushButton("Run Bash File2", self)
        self.pushBtn1.move(375, 100)
        self.pushBtn2.move(375, 250)
        self.pushBtn1.clicked.connect(self.fun_bash1)
        self.pushBtn2.clicked.connect(self.fun_bash2)

        layout.addWidget(self.pushBtn1)
        layout.addWidget(self.pushBtn2)

    def fun_bash1(self):
        os.system("./bash1.py")

    def fun_bash2(self):
        os.system("./bash2.py")

class video_player(QWidget):
    # Video Player Main widget
    def __init__(self):
        super().__init__()
        self.second_call = SecondWindow()
        self.mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)

        btnSize = QSize(16, 16)
        videoWidget = QVideoWidget()

        # taskbar open-video button
        openButton = QPushButton("Open Video")
        openButton.setToolTip("Open Video File")
        openButton.setStatusTip("Open Video File")
        openButton.setFixedHeight(24)
        openButton.setIconSize(btnSize)
        openButton.setFont(QFont("Noto Sans", 8))
        openButton.setIcon(QIcon.fromTheme("document-open", QIcon("D:/_Qt/img/open.png")))
        # openButton.clicked.connect(self.abrir)
        self.Video()

        # taskbar play button
        self.playButton = QPushButton()
        self.playButton.setEnabled(False)
        self.playButton.setFixedHeight(24)
        self.playButton.setIconSize(btnSize)
        self.playButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.playButton.clicked.connect(self.play)

        self.positionSlider = QSlider(Qt.Horizontal)
        self.positionSlider.setRange(0, 0)
        self.positionSlider.sliderMoved.connect(self.setPosition)

        self.statusBar = QStatusBar()
        self.statusBar.setFont(QFont("Noto Sans", 7))
        self.statusBar.setFixedHeight(14)

        controlLayout = QHBoxLayout()
        controlLayout.setContentsMargins(0, 0, 0, 0)
        controlLayout.addWidget(openButton)
        controlLayout.addWidget(self.playButton)
        controlLayout.addWidget(self.positionSlider)

        layout = QVBoxLayout()
        layout.addWidget(videoWidget)
        layout.addLayout(controlLayout)
        layout.addWidget(self.statusBar)

        self.setLayout(layout)

        self.mediaPlayer.setVideoOutput(videoWidget)
        self.mediaPlayer.stateChanged.connect(self.mediaStateChanged)
        self.mediaPlayer.positionChanged.connect(self.positionChanged)
        self.mediaPlayer.durationChanged.connect(self.durationChanged)
        self.mediaPlayer.error.connect(self.handleError)
        # self.statusBar.showMessage("Ready")

    def Video(self):
        # fileName, _ = QFileDialog.getOpenFileName(self, "Selecciona los mediose",
        #         ".", "Video Files (*.mp4 *.flv *.ts *.mts *.avi)")
        fileName = "/home/wot-param/Documents/Param/1. Section Intro.mp4"

        if fileName != '':
            self.mediaPlayer.setMedia(
                QMediaContent(QUrl.fromLocalFile(fileName)))
            # self.playButton.setEnabled(True)
            # self.statusBar.showMessage(fileName)
            self.play()

    def play(self):
        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.mediaPlayer.pause()
        else:
            self.mediaPlayer.play()

    def mediaStateChanged(self, state):
        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.playButton.setIcon(
                self.style().standardIcon(QStyle.SP_MediaPause))
        else:
            self.playButton.setIcon(
                self.style().standardIcon(QStyle.SP_MediaPlay))

    def positionChanged(self, position):
        self.positionSlider.setValue(position)
        if durationChek("/home/wot-param/Documents/Param/1. Section Intro.mp4") <= position:
            self.second_call.show()
            QtCore.QTimer.singleShot(8000, self.second_call.close)
            QtCore.QTimer.singleShot(7900, self.Video)
        else:
            None

    def durationChanged(self, duration):
        self.positionSlider.setRange(0, duration)

    def setPosition(self, position):
        self.mediaPlayer.setPosition(position)

    def handleError(self):
        self.playButton.setEnabled(False)
        self.statusBar.showMessage("Error: " + self.mediaPlayer.errorString())

#Media player main Widget
if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    player = video_player()
    player.setWindowTitle("Player")
    player.resize(600, 400)
    player.show()
    sys.exit(app.exec_())
