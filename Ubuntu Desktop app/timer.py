import sys

from PyQt5.QtGui import QIcon, QFont
from PyQt5.QtCore import QDir, Qt, QUrl, QSize
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer
from PyQt5.QtMultimediaWidgets import QVideoWidget
from PyQt5.QtWidgets import (QApplication, QFileDialog, QHBoxLayout, QLabel,
                             QPushButton, QSizePolicy, QSlider, QStyle, QVBoxLayout, QWidget, QStatusBar, QMainWindow)
from PyQt5 import QtCore
from time import sleep
from random import getrandbits


def timeout():
    print('timeout, elapsed: {}'.format(
        elapsed.elapsed() * .001))
    elapsed.start()
    if getrandbits(1):
        print('waiting one second')
        sleep(1)
    else:
        print('no wait')

app = QtCore.QCoreApplication(sys.argv)
elapsed = QtCore.QElapsedTimer()
elapsed.start()
timer = QtCore.QTimer(interval=250, timeout=timeout)
timer.start()
app.exec_()