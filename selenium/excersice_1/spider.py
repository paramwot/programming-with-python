#Few required Libraries
from selenium import webdriver
import pandas as pd
from selenium.common.exceptions import NoSuchElementException

path = "/home/wot-param/Documents/Param/paramteraiya/April/selenium/chromedriver" #path to cromedriver
driver = webdriver.Chrome(path) #creating instance of webdriver

driver.get('http://quotes.toscrape.com/') #website

def dataScrapping():
    '''A function which extracts data from HTML page'''
    text_list = driver.find_elements_by_class_name('text')  # list of quotes
    author_list = driver.find_elements_by_class_name('author')  # list of authors
    tags_list = driver.find_elements_by_class_name('tags')  # list of tags
    quote_csv = []

    for (text, author, tag) in zip(text_list, author_list, tags_list):
        quote_info = [text.text, author.text, tag.text]  # variable to store instantenious value
        quote_csv.append(quote_info)  # appending to 2d array
    return quote_csv


quote_csv = dataScrapping() #calling a function
next_btn = driver.find_element_by_xpath('//li[@class="next"]/a')
next_btn.click() #page-next btn

while True:
    try:
        quote_csv += dataScrapping() #2d array for storing extracted data
        next_btn = driver.find_element_by_xpath('//li[@class="next"]/a') #page-next btn
        next_btn.click()
    except NoSuchElementException:
        break

dataframe = pd.DataFrame(quote_csv) #Generating DataFrame
dataframe.columns = ['Quote','Author', 'Tags'] #Column names of DataFrame file
dataframe.to_csv('dataframe.csv') #enerating csv outof dataframe

driver.close() #Closing crome obj

