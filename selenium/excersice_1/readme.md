#Choose a website to scrap & Generate Data Analysis

### URL for scraping website: http://quotes.toscrape.com/
This module directory contains two script: 
* spider.py 
* analysis.ipynb

#### - spider.py file contains a selenium based web scrapper.
* It extracts text data from HTML tags
and dumps to csv file

#### -analysis.ipynb file is a jupyter notebook for text analysis
* It generates few graph:
  * "tags_word_cloud.png" file shows the genra used in the quotes.
    * Higher the size of text indecates the redundancy of the topic. 
    * It shows majority of authors has written their quotes in that perticular genra.
    
  * "Histogram_authors_count.png" file show the histogram between number of authors Vs. Number of quotes
    * This graph shows the number of quotes writen by respective authors.
    * There is less than 5 authors has written 10 quotes and majority of authors(40) has written onw quote.

