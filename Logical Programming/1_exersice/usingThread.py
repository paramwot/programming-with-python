import multiprocessing
import threading
import time
n = 5
def if_fun(i,j):
    if abs(j) > n or abs(i) > n or i == 0 and j == 0:
        print("0", end=' ')

def elif_fun(i,j):
    if abs(j) > n or abs(i) > n or i == 0 and j == 0:
        pass
    elif ((j>0 and i<0 or j<0 and i>0) and abs(j)>=abs(i)) or ((i<0 and j<0 or i>0 and j>0) and abs(j)<=abs(i)):
        print(abs(j),end=" ")
    else:
        print(" ",end=" ")

if __name__=='__main__':
    start = time.time()
    for i in range(-n-1,n+2):
        for j in range(-n-1,n+2):
            process1 = threading.Thread(target=if_fun,args=(i,j))
            process1.start()
            process1.join()
            # if abs(j)>n or abs(i)>n or i==0 and j==0:
            #     print("0",end=' ')
            process2 = threading.Thread(target=elif_fun,args=(i,j))
            process2.start()
            process2.join()
            # elif (j>0 and i<0 or j<0 and i>0) and abs(j)>=abs(i):
            #     print(abs(j),end=" ")
            # elif (i<0 and j<0 or i>0 and j>0) and abs(j)<=abs(i):
            #     print(abs(j),end=" ")
            # else:
            #     print(" ",end=" ")
        print(" ",end="\n")
    print(time.time()-start)