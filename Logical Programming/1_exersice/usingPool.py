import time
import multiprocessing
def pattern(n):
    for i in range(-n-1, n+2):
        for j in range(-n-1, n+2):
            if (abs(j)>n or abs(i)>n) or (i==0 and j==0):
                print("0",end=' ')
            elif ((j>0 and i<0 or j<0 and i>0) and abs(j)>=abs(i)) or ((i<0 and j<0 or i>0 and j>0) and abs(j)<=abs(i)):
                print(abs(j),end=" ")
            else:
                print(" ",end=" ")
        print(" ",end="\n")

if __name__ == '__main__':

    start = time.time()
    p = multiprocessing.Pool()
    answer = p.map(pattern,range(1000,1001))
    p.close()
    p.join()
    print(time.time() - start)