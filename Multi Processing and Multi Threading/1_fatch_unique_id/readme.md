### Problem Statement: 
* Fetch unique id from https://www.uuidgenerator.net/api/version1
* input: n=5
* output:
ba42e844-87c5-11eb-8dcd-0242ac130003
284ebfb6-87c6-11eb-8dcd-0242ac130003
2b18f6e4-87c6-11eb-8dcd-0242ac130003
2d084978-87c6-11eb-8dcd-0242ac130003
2f0423dc-87c6-11eb-8dcd-0242ac130003
  
* Here, used "request" module for API call and getting responce in a function with the name "fetch"
* Inorder to reduce execution time, have used Parallelism; which significantly reduces the execution time. 