import asyncio
import requests
async def fetch_id():
    response = requests.get("https://www.uuidgenerator.net/api/version1")
    print(response.text)

async def main():
    for _ in range(n):
        await asyncio.gather(fetch_id())

if __name__ == "__main__":
    import time
    n = int(input("Enter n: "))
    start = time.time()
    asyncio.run(main())
    print("Total time:",time.time()-start)