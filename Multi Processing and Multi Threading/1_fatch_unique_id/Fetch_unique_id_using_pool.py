import requests
from multiprocessing import Pool
import time
def fetch(input_user):
    response = requests.get("https://www.uuidgenerator.net/api/version1")
    print(response.text)

if __name__ == "__main__":
    input_user = int(input("Enter input: "))
    start = time.time()
    p = Pool(60)
    p.map(fetch,range(input_user))
    print("Total Time: ", time.time()-start)
