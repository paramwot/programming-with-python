import time
import threading

def cal_square(numbers):
    print("squaring numbers\n")
    for n in numbers:
        time.sleep(0.2)
        print("Square:", n * n)

def cal_cube(numbers):
    print("Cubing numbers\n")
    for n in numbers:
        time.sleep(0.2)
        print("Cube:", n * n * n)


arr = [2, 3, 4, 5]

t = time.time()
# cal_square(arr)
# cal_cube(arr)
t1 = threading.Thread(target=cal_square,args=(arr,))
t2 = threading.Thread(target=cal_cube,args=(arr,))

t1.start()
t2.start()
t1.join()
t2.join()
print("Total Time: ", time.time()-t)
