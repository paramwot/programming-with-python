# sharing a variable in process
#Data exchange could be happer with "value" and "array" in multi-processing
import multiprocessing

def cal_square(numbers,answer,val):
    val.value = 5.2
    for idx, n in enumerate(numbers):
        answer[idx] = n*n

if __name__=="__main__":
    numbers = [2, 3, 4]
    answer = multiprocessing.Array('i',3) #Array has two attributes; first is variable tupe and second for size
    val = multiprocessing.Value('d',0.0)
    process1 = multiprocessing.Process(target=cal_square,args=(numbers,answer,val))

    process1.start()
    process1.join()

    print(answer[:])
    print(val.value)
