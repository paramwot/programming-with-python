import multiprocessing
import time

def f(n):
    sum = 0
    for x in range(1000):
        sum += x*x
    return sum

if __name__=="__main__":
    t1 = time.time()
    p = multiprocessing.Pool()
    answer = p.map(f,range(10000))
    p.close()
    p.join()

    print("Pool took: ",time.time()-t1)

    t2 = time.time()
    answer = []
    for x in range(10000):
        answer.append(f(x))

    print("serial Processing took: ",time.time()-t2)