'''
Every process has its own address space(Virtual memory).
Thus program variables are not shared between two process.
You need to use inter process communication (IPC) techniques
if you want to share data between two processes.
'''

#Benifit of multiprocess is that error or memory leak in one process won't hurt execution of another process.

import time
import multiprocessing
ans = []
def cal_square(numbers):
    global ans
    for n in numbers:
        time.sleep(0.2)
        print("Square "+ str(n*n))
        ans.append(n*n)
    print(ans)

def cal_cube(numbers):
    for n in numbers:
        time.sleep(0.2)
        print("Cube "+ str(n*n*n))

if __name__ == "__main__":
    t = time.time()
    arr = [2, 3, 4, 5]
    process1 = multiprocessing.Process(target=cal_square,args=(arr,))
    process2 = multiprocessing.Process(target=cal_cube,args=(arr,))

    process1.start()
    process2.start()
    process1.join()
    process2.join()
    print("Total time:",time.time()-t)