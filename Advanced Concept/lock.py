import time
import multiprocessing

def deposite(balance,lock):
    for i in range(100):
        lock.acquire()
        balance.value = balance.value + 1
        lock.release()

def withdraw(balance,lock):
    for i in range(100):
        lock.acquire()
        balance.value = balance.value - 1
        lock.release()

if __name__=='__main__':
    balance = multiprocessing.Value('i',200)
    lock = multiprocessing.Lock()
    process1 = multiprocessing.Process(target=deposite,args=(balance,lock))
    process2 = multiprocessing.Process(target=withdraw,args=(balance,lock))

    process1.start()
    process2.start()
    process1.join()
    process2.join()

    print(balance.value)

