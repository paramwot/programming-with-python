from sumOfTupleElem_run import ClassRun
import json
file = open("test_cases.json", 'r').read()
json_file = json.loads(file)
class_obj = ClassRun()
output_file = open("output.txt", "a")
output_file.truncate(0)
for i in json_file['case']:
    if i['output'] == class_obj.run_file(tuple(i['input'])):
        output_file.write("Correct\n")
    else:
        output_file.write("Not Correct\n")
output_file.close()