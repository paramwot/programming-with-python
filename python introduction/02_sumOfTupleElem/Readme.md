## Exercise: Sum of Tuple Elements
* Training Name: "Programming with Python"
* Module Name: "Python Introduction"
* Exercise Number: "2"
* Problem Statement:
Write a Python program to add two positive integers in tupple without using the '+' operator.
Example:
Input=>(2,3) Output=> 5
Input=>(4,6) Output=>10

##### convert tuple into list and apply python inbuilt fun "SUM"

## To run this file:
* Step-1: Enter Your Test case in "test_case.txt" file 
(e.g: (2,3)@7
(4,6))@10
)
* Step-2: run "sumOfTupleElem_test.py" 
* Step-3: check "Correct"/"Incorrect" output in "output.txt" file