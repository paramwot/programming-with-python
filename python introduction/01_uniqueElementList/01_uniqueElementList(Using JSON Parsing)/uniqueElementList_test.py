import json
from uniqueElementList_run import Class_Run
file = open("test_cases.json", 'r').read()
json_file = json.loads(file)
class_obj = Class_Run()
output_file = open("output.txt","a")
output_file.truncate(0)
[i['output'] == class_obj.run_file(i['input']) if output_file.write("Correct\n") else output_file.write(" Not Correct\n") for i in json_file['case']]
output_file.close()
# for i in json_file['case']:
#     i['output'] == class_obj.run_file(i['input']) if output_file.write("Correct\n") else output_file.write(" Not Correct\n")
