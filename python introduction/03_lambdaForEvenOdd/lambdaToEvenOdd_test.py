from lambdaToEvenOdd_run import Even_Odd
testCaseFile = open("test_cases.txt", "r").read().split("\n")
new_f,case_ans, user_ans = [], [], []
for i in testCaseFile:
    new_f.append(i.split("@")[0])
    case_ans.append(i.split("@")[1])
# print(new_f)
# print(case_ans)
class_obj = Even_Odd()
for line in new_f:
    line = list(map(int,line.replace("[", "").replace("]", "").split(",")))
    user_ans.append(class_obj.evenOdd(line))
print(case_ans)
print(user_ans)
output_file = open("output.txt", "a")
output_file.truncate(0)
for i in range(len(case_ans)):
    if case_ans[i] == str(user_ans[i]):
        print("Correct")
        output_file.write("Correct\n")
    else:
        print("Not")
        output_file.write("Not Correct\n")
output_file.close()