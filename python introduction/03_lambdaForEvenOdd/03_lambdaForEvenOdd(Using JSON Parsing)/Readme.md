### Exercise: Write a Python program to filter(even and odd) a list of integers using Lambda.
#### Example: Input=>[1, 2, 3, 4, 5, 6, 7, 8, 9, 10] Output=> [2, 4, 6, 8, 10] and [1, 3, 5, 7, 9]

* Logic: mapped even number from Input list using Lambda 

#### To run this file:
* Step-1: Enter Your Test case in "test_case.txt" file 
(e.g: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]@([2, 4, 6, 8, 10], [1, 3, 5, 7, 9]))
* Step-2: run "lambdaToEvenOdd_test.py" 
* Step-3: check "Correct"/"Incorrect" output in "output.txt" file