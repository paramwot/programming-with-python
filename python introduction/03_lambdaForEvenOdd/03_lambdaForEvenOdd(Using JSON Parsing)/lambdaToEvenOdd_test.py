import json
from lambdaToEvenOdd_run import Even_Odd
file = open("test_cases.json",'r').read()
json_file = json.loads(file)
output_file = open("output.txt", "a")
output_file.truncate(0)
class_obj = Even_Odd()
for i in json_file['case']:
    # user_ans.append(class_obj.evenOdd(json_file['case'][i]['input']))
    if i['output'] == list(class_obj.evenOdd(i['input'])):
        output_file.write("Correct\n")
    else:
        output_file.write("Not Correct\n")
output_file.close()
