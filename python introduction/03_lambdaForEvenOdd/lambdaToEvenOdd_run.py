class Even_Odd():
    def evenOdd(self,input_list):
        even_list, odd_list=[], []
        temp_list= list(map(lambda x: x if x%2==0 else None,input_list))
        [even_list.append(i) for i in temp_list if i!=None]
        temp_list=[]
        temp_list= list(map(lambda x: x if x%2!=0 else None,input_list)) 
        [odd_list.append(i) for i in temp_list if i!=None]
        return even_list,odd_list
# class_obj = Even_Odd()
# print(class_obj.evenOdd([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
