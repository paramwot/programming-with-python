import json
from sortedByLastEle_run import sortTuple
file = open("test_cases.json",'r').read()
json_file = json.loads(file)
new_json_file = []
output_file = open("output.txt","a")
output_file.truncate(0)
class_obj=sortTuple()
for i in json_file['case']:
    if class_obj.sort_by_lats(list(map(tuple,i['input']))) == list(map(tuple,i['output'])):
        print("Correct")
        output_file.write("Correct\n")
    else:
        print("No Correct")
        output_file.write(" Not Correct\n")
output_file.close()