import datetime
class lambda_date():
    def date_lambda(self,input_str):
        date_list = []
        date_time_obj = datetime.datetime.strptime(input_str.strip(" \t\r\n"), '%Y-%m-%d %H:%M:%S.%f')
        year = lambda x: x.year
        month = lambda x: x.month
        day = lambda x: x.day
        hour = lambda x: x.hour
        minute = lambda x: x.minute
        second = lambda x: x.second
        microsecond = lambda x: x.microsecond
        date_list.append(str(year(date_time_obj)))
        date_list.append(str(month(date_time_obj)))
        date_list.append(str(day(date_time_obj)))
        date_list.append(str(hour(date_time_obj)))
        date_list.append(str(minute(date_time_obj)))
        date_list.append(str(second(date_time_obj))+'.'+str(microsecond(date_time_obj)))
        return date_list 
        
# class_obj = lambda_date()
# print(class_obj.date_lambda('2020-01-15 09:03:32.744178'))
