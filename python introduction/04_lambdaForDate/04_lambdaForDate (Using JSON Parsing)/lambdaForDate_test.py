import json
from lambdaForDate_run import lambda_date
file = open("test_cases.json",'r').read()
json_file = json.loads(file)
class_obj = lambda_date()
output_file = open("output.txt", "a")
output_file.truncate(0)
for i in json_file['case']:
    if i['output'] == class_obj.date_lambda(i['input']):
        output_file.write("Correct\n")
    else:
        output_file.write("Not Correct\n")
output_file.close()