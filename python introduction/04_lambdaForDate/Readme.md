Exercise:
Training Name: "Programming with Python"
Module Name: "Python Introduction"
Exercise Number: "4"
Problem Statement:
Write a Python program to extract the year, month, date, and time using Lambda.
Example:
Input => 2020-01-15 09:03:32.744178
Output =>Y=2020 => M=1 => D=15 => H=09 => M=03 => S=32.744178

Logic =>  converted user string input to Datetime object, inorder to pass different perameters. Using Lambda function, got the value with respect to year, month, date, and time(hour,min,sec,microseconds) and pushed into datetime array. 

To run this file:
Step-1: Enter Your Test case in "test_case.txt" file("MAINTAIN PROPER SPACING AFTER COMMA!")
Step-2: Step-2: run "sumOfTupleElem_test.py" 
Step-3: check "Correct"/"Incorrect" output in "output.txt" file