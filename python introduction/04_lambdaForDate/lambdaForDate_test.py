from lambdaForDate_run import lambda_date
testCaseFile = open("test_cases.txt", "r").read().split("\n")
new_f, case_ans, user_ans = [], [], []
for i in testCaseFile:
    new_f.append(i.split("@")[0])
    case_ans.append(i.split("@")[-1])
class_obj = lambda_date()
for line in new_f:
    user_ans.append(class_obj.date_lambda(line))
# print(case_ans[0].replace('[','').replace(']','').strip(" "))
# print(str(user_ans[0]).replace('[','').replace(']','').strip(" "))
output_file = open("output.txt", "a")
output_file.truncate(0)
for i in range(len(case_ans)):
    if case_ans[i].replace('[','').replace(']','').strip(" ") == str(user_ans[i]).replace('[','').replace(']','').strip(" "):
        print("Correct")
        output_file.write("Correct\n")
    else:
        print("No Correct")
        output_file.write("Not Correct\n")
output_file.close()