#Parent class
from abc import ABC,abstractmethod
class Animal(ABC):
    number_of_legs = 4
    def legs_coult(self):
        return self.number_of_legs

    __message = "This is ANIMAL CLASS"

    # @abstractmethod
    def get_info(self):
        return self.__message

#child class
class Wild(Animal):
    __message = "This is WILD CLASS"
    __food = "Wild Food"

    def food(self):
        return self.__food

    def get_info(self):
        return Animal.get_info(self),self.__message

class Pet(Animal):

    __message = "This is PET CLASS"
    __food = "Pet Food"

    def food(self):
        return self.__food

    # @abstractmethod
    def get_name(self):
        return

    def get_info(self):
        return Animal.get_info(self),self.__message

class Canine(Animal):
    __message = "This is Canine CLASS"
    __food = "Canine Food"

    def food(self):
        return self.__food

    def get_info(self):
        return Animal.get_info(self), self.__message

class Feline(Animal):
    __message = "This is Feline CLASS"
    __food = "Feline Food"

    def food(self):
        return self.__food

    def get_info(self):
        return Animal.get_info(self), self.__message

#child-child class
class Leopard(Wild):

    __message = "This is LEOPARD CLASS"
    __name = "Bagira"

    def get_info(self):
        return Wild.get_info(self),self.__message,self.__name

class Fox(Wild,Canine):

    __message = 'THIS IS FOX CLASS'
    __name = 'harshal'

    def get_info(self):
        return Wild.get_info(self),Canine.get_info(self)[1],self.__message,self.__name

class Dog(Pet,Canine):
    __message = 'THIS IS dog CLASS'
    __name = 'param'
    def get_name(self):
        return self.__name
    def get_info(self):
        return Pet.get_info(self), Canine.get_info(self)[1], self.__message, self.__name

class Cat(Pet,Feline):
    __message = 'THIS IS cat CLASS'
    __name = 'sandip'
    def get_name(self):
        return self.__name

    def get_info(self):
        return Pet.get_info(self), Feline.get_info(self)[1], self.__message, self.__name

#instances
animal_obj = Animal()
wild_obj = Wild()
pet_obj = Pet()
canine_obj = Canine()
leopard_obj = Leopard()
dog_obj = Dog()
cat_obj = Cat()
fox_obj = Fox()
print(dog_obj.get_info())