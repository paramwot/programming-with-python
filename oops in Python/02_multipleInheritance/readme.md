##### Exercise: 
* Use Following class diagram to create classes.
* Create a simple message flow to get flow from all the classes to dog,cat,leopard and fox

#### Logic:
* create parent class named Animal() 
* create sub class names Wild(),Pet(),Canine(),Feline()
* create sub class with the following inheritance
* Wild => Leapord(),FoX()
* Pet => Dog(),Cat()
* Canine => Dog(),Fox()
* Feline => Cat(),Leapord()
* On calling the child class, Should access all the parent class with their messages

