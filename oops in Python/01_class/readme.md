##### Write a Class which has all functionality of a list. (here is all functionality )[https://www.w3schools.com/python/python_ref_list.asp]
###### ==>create a method append which only appends values which are divisible by 5. Else raise an exception NotDivisibleBy5. While appending divide that value by 5 
###### =>This contains another method which lets me check that all elements in the array are even.

### Logic:
* Inherited the list class to sub-class "CustomList()", and override an append method;
* This Append method only appends the element which is divisible by 5.

### Example:
* Input => (This is single input and output Structure)
* custom_list_obj=CustomList()
* custom_list_obj.append(6) //(raise exception) continue even after exception to interpret new line
* print(custom_list_obj)
* custom_list_obj.append(5) //add 1 in list because 5/5=1
* print(custom_list_obj)
* custom_list_obj.append(10) //add 2 in list because 10/5=2 
* print(custom_list_obj)
* custom_list_obj.insert(0,8) //insert 8 value at index 0 
* print(custom_list_obj)
* print(custom_list_obj.is_all_even())

* Output =
* []
* [1]
* [1,2]
* [8,1,2]
* [False]